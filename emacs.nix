/*
This is a nix expression to build Emacs and some Emacs packages I like
from source on any distribution where Nix is installed. This will install
all the dependencies from the nixpkgs repository and build the binary files.
*/

{ pkgs ? import <nixpkgs> {} }: 

let
  myEmacs = (pkgs.emacs.override {
    # Use gtk3 instead of the default gtk2
    withGTK3 = true;
    withGTK2 = false;
  });
  emacsWithPackages = (pkgs.emacsPackagesGen myEmacs).emacsWithPackages; 
in
  emacsWithPackages (epkgs: (with epkgs.melpaStablePackages; [ 
  ]) ++ (with epkgs.melpaPackages; [

    use-package

    ledger-mode

    magit
    git-timemachine
    diff-hl

    rust-mode cargo
    nix-mode
    haskell-mode
    geiser ac-geiser paredit
    
    ivy
    ivy-bibtex
    emms
    pdf-tools
    restart-emacs
    hydra
    yasnippet
    yasnippet-snippets
    fcitx
    
    
    org-bullets
    fill-column-indicator
    which-key
    ace-window
    anzu
    ace-window
    rainbow-delimiters
    company
    flycheck flycheck-rust flycheck-haskell flycheck-ledger

    emojify
    hide-mode-line
    all-the-icons
        
    gruvbox-theme
    material-theme 
    panda-theme
    monokai-theme
    dracula-theme

    scribble-mode
    spaceline
    nov

    org-ref
    projectile

    
  ]) ++ (with epkgs.elpaPackages; [
    auctex
  ])++ (with epkgs.orgPackages; [
    org
  ])++ [
        pkgs.notmuch   # From main packages set

  ])
