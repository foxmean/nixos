# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Manual C-M-F8
  services.nixosManual.showManual = true;
  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  networking.hostName = "nixos_hpg32"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp2s0.useDHCP = true;
  networking.interfaces.wlp3s0b1.useDHCP = true;

  # network-manager
  networking.networkmanager.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_US.UTF-8";
  };

  # Set your time zone.
  time.timeZone = "Asia/Bangkok";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  
  environment.systemPackages = with pkgs; [
    # Computer languages
    (python3.withPackages(ps: with ps; [ scipy numpy toolz seaborn matplotlib pandas bokeh ]))
    guile
    gcc
    ditaa plantuml asymptote gnuplot
    # Identity
    gnupg automake ghostscript gv texinfo
    # Need to basic usaage
    networkmanager
    pavucontrol
    fcitx fcitx-engines.mozc
    fcitx-engines.table-extra fcitx-engines.table-other
    zip unzip
    # Basic commandline tools
    nitrogen graphviz tree aspell wmctrl xdotool
    git wget curl tmux cmus p7zip feh imagemagick
    xorg.xmessage scrot xclip file pandoc ditaa
    gtypist youtube-dl rtorrent mpv ffmpeg
    ledger hledger
    tig
    vim
    parted
    wkhtmltopdf
    # Avant-garde engine
    ipfs tor pijul
    # Some basic GUI
    firefox brave tor-browser-bundle-bin
    libreoffice-fresh gpodder vlc
    gimp krita
    calibre okular 
    transmission-gtk webtorrent_desktop
    rofi
    screenkey
    tdesktop
    quaternion
    anki
    flameshot
    hexchat
    zotero
    gnome3.cheese
    protonmail-bridge
    mcomix

    # Theme
    papirus-icon-theme
    mojave-gtk-theme
    nordic
    numix-gtk-theme
    theme-jade1

    (import ./emacs.nix { inherit pkgs; })

    ### For LaTeX and Beamer
    (texlive.combine {
        inherit (texlive) scheme-full
       ;
     })
   ### End For LaTeX and Beamer
  ];
  
  ###############################
  ## Input Method Editor (IME) ##
  ###############################

  # This enables "fcitx" as your IME.  This is an easy-to-use IME.  It supports many different input methods.
  i18n.inputMethod.enabled = "fcitx";

  # This enables "mozc" as an input method in "fcitx".  This has a relatively
  # complete dictionary.  I recommend it for Japanese input.
  i18n.inputMethod.fcitx.engines = with pkgs.fcitx-engines; [ mozc ];

   ###################
  ## Font Settings ##
  ###################

  # Enable fonts to use on your system.  You should make sure to add at least
  # one English font (like dejavu_fonts), as well as Japanese fonts like
  # "ipafont" and "kochi-substitute".
  fonts.fonts = with pkgs; [
    tlwg
    noto-fonts
    noto-fonts-extra
    carlito
    dejavu_fonts
    ipafont
    kochi-substitute
    source-code-pro
    ttf_bitstream_vera
    font-awesome
    siji                              # Iconic bitmap font
    symbola                           # Braille support for gotop command
    hack-font
    comic-relief
  ];

  # Enable the "ultimate" font config.  This enables a few extra options to
  # make sure fonts look nice.  However, if you enable this and fonts look
  # strange, try disabling it.
  fonts.fontconfig.ultimate.enable = true;

  # These settings enable default fonts for your system.  This setting is very
  # important.  It lets fontconfig know that you want to fall back to a Japanese
  # font (for example "IPAGothic") if an application tries to show fonts with
  # Japanese.  For instance, this is important if you are using a terminal
  # emulator and you `cat` some Japanese text to the screen. If you don't have
  # "defaultFonts" configured, fontconfig will pick a random Japanese font to
  # use.  If you have this "defaultFonts" setting configured, fontconfig will
  # pick the font you have selected.  This makes sure Japanese fonts look nice.
  fonts.fontconfig.defaultFonts = {
    monospace = [
      "Hack"
      "DejaVu Sans Mono"
      "IPAGothic"
      "Tlwg Typist"
      "Noto Mono"
    ];
    sansSerif = [
      "DejaVu Sans"
      "IPAPGothic"
      "Loma"
      "Noto Sans"
    ];
    serif = [
      "DejaVu Serif"
      "IPAPMincho"
      "Waree"
      "Noto Serif"
    ];
  };
  
  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:
  
  # Tor
  services.tor.enable = true;
  services.tor.client.enable = true;
  
  # Emacs
  services.emacs.defaultEditor = true;
  services.emacs.enable = true;
  services.emacs.install = true;
  services.emacs.package = import ./emacs.nix { inherit pkgs; };

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable Virtualbox  
  virtualisation.virtualbox.host.enable = true;
  users.extraGroups.vboxusers.members = [ "user-with-access-to-virtualbox" ];

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound.
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  services.xserver.libinput.enable = true;

  # Enable displayManager
  services.xserver.displayManager.sddm.enable = true;

  # Enable the xfce Desktop Environment.
  services.xserver.desktopManager.xfce.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.meanix = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "vboxusers" ];
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}
  # configuration.nix end here
